import React from "react";

import { Container, Row, Col } from "reactstrap";
import Link from "../components/link";
import Button from "../components/btn";
import Layout from "../components/layout";
import SEO from "../components/seo";
import { FaGithub, FaBolt, FaHome, FaWrench } from "react-icons/fa";
import Form from "../components/form";
import Slider from "../components/slider";
import Box from "../components/box";
import Hr from "../components/hr";
import Benefits from "../components/benefits";
import styled from "styled-components";
import HomeFeatures from "../components/homeFeatures";
import Testimonials from "../components/testimonials";

let StyledBackground = styled.div`
  background: linear-gradient(to bottom, #f9fbfd 0, #fff 100%);
`;

let Service = ({ title, Icon = FaHome }) => (
  <Col xs={12} md={4} className="mb-3">
    <Link to="/">
      <Box>
        <Icon size={30} />
        <h4 className="mt-3">{title}</h4>
      </Box>
    </Link>
  </Col>
);

let Index = () => (
  <Layout>
    <SEO title="Home" />
    <Slider />
    <Container className="pt-4">
      <div className="text-center">
        <h4>About Us</h4>
        <p className="text-muted">
          EzLoct is a consortium of young and dynamic people, high on enthusiasm
          and abounding in information. EzLoct believes in helping clients bring
          their business online and empowering organizations to gain a
          competitive advantage in the New Economy. the firm’s past, present and
          future is built on mastering evolving technologies quickly, to
          maximize client value By judicious blend of business analysis &
          management with latest technology, 9X offers the most advanced
          solutions by developing state-of-the-art and custom software, Website
          developing & web based applications. With a complete spectrum of
          services on offer (high-traffic scalable B2B and B2C web solutions,
          advanced e-commerce solutions, and dynamic web applications), Always
          pushing the possibilities, EzLoct is at the forefront of emerging
          technologies.
        </p>
      </div>
    </Container>

    <Container className="pt-4">
      <div className="text-center">
        <h4>Products</h4>
        <p className="text-muted">
          Boliyo is a shopping cart application. We can provide products at the
          lowest possible cost in nearby areas by using this app. Design that is
          simple and easy to use.
        </p>
      </div>
    </Container>
    <StyledBackground>
      <Benefits />
    </StyledBackground>
    <HomeFeatures />
  </Layout>
);

export default Index;
