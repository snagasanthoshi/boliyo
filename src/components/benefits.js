import React from "react";
import { Container, Row, Col } from "reactstrap";
import { FaCode } from "react-icons/fa";
import { StaticQuery, graphql } from "gatsby";
import Img from "gatsby-image/withIEPolyfill";
import styled from "styled-components";

let StyledImg = styled((props) => <Img {...props} />)`
  perspective: 1500px;
  perspective-origin: left center;
  overflow: visible !important;
  picture,
  img {
    transform: rotateY(-35deg) rotateX(15deg);
    box-shadow: 25px 60px 125px -25px rgba(80, 102, 144, 0.1),
      16px 40px 75px -40px rgba(0, 0, 0, 0.2);
    border-radius: 0.625rem;
    transition: 1s !important;
    &:hover {
      transform: rotateY(-30deg) rotateX(15deg);
    }
  }
`;

let Benefit = ({ title, content }) => (
  <div className="d-flex mb-4">
    <FaCode size={30} className="primary" />
    <div className="ml-3">
      <h4>{title}</h4>
      <p className="m-0 text-muted">{content}</p>
    </div>
  </div>
);

let Benefits = ({ data }) => (
  <Container className="py-5">
    <Row className="d-flex align-items-center">
      <Col md="8">
        <div className="mb-4">
          <h2 className="primary">What makes Boliyo interesting.. </h2>
          <p className="text-muted">Blazing fast. Just try it.</p>
        </div>
        <Benefit
          title="Affordable & Accessible"
          content="Providing affordable & accessible learning during the current pandemic times!"
        />
        <Benefit
          title="Translated Content"
          content="Content consumable in a variety of languages (automatically translated)!"
        />

        <Benefit
          title="Intuitive UI"
          content="Easy addition of Products and place the order."
        />
      </Col>
      <Col md="4">
        <StyledImg
          fluid={data.file.childImageSharp.fluid}
          objectFit="contain"
          objectPosition="50% 50%"
        />
      </Col>
    </Row>
  </Container>
);

const BenefitsQuery = () => (
  <StaticQuery
    query={graphql`
      query BenefitsQuery {
        file(relativePath: { eq: "sample.png" }) {
          id
          childImageSharp {
            fluid {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    `}
    render={(data) => <Benefits data={data} />}
  />
);

export default BenefitsQuery;
